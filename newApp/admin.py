from django.contrib import admin
from .models import Question, Choice

class ChoiceInLine(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes':
['collapse']}),
    ]
    inlines = [ChoiceInLine] #для добавления объектов зависимых сущностей
    list_display = ('question_text', 'pub_date', 'was_published_recently') #какие поля отображаются
    list_filter = ['pub_date'] #добавляет интерфейс для фильтрации опросов
    search_fields = ['question_text'] #поле для поиска вопросов по их тексту

admin.site.register(Question, QuestionAdmin)

# Register your models here.
