from django.contrib import admin
from django.urls import include, path
from . import views

app_name='newApp'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    #/newapp/5/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    #/newapp/5/results
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]