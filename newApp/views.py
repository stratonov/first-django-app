from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
import datetime

from .models import Question, Choice
from django.shortcuts import render, get_object_or_404


class IndexView(generic.ListView):
    template_name = 'newApp/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the las five published questions."""
        return Question.objects.filter(pub_date__lte=datetime.datetime.now()).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'newApp/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=datetime.datetime.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'newApp/results.html'


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'newApp/detail.html', {'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'newApp/results.html', {'question': question})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'newApp/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('newApp:results', args=(question.id, )))

